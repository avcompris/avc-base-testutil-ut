# About avc-base-testutil-ut


This project runs tests against classes in the avc-base-testutil project.
		
Its parent project is 
[avc-base-parent](https://gitlab.com/avcompris/avc-base-parent/).
	
Project dependencies include:
	
* [avc-base-testutil](https://gitlab.com/avcompris/avc-base-testutil/)

This is the project home page, hosted on GitLab.

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-base-testutil-ut/)
