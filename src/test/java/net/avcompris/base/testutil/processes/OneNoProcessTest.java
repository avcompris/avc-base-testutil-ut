package net.avcompris.base.testutil.processes;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

public class OneNoProcessTest {

	@Test
	public void testOneNoProcess() throws Exception {

		final Result result = JUnitCore.runClasses(OneNoProcess.class);

		assertEquals(0, result.getFailureCount());

		assertEquals(0, result.getIgnoreCount());

		assertEquals(1, result.getRunCount());
	}

	@Test
	public void testOneNoProcessCrash() throws Exception {

		final Result result = JUnitCore.runClasses(OneNoProcessCrash.class);

		assertEquals(1, result.getFailureCount());

		assertEquals(0, result.getIgnoreCount());

		assertEquals(1, result.getRunCount());
	}
}
