package net.avcompris.base.testutil.processes;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

public class AllScmIgnoresProcessTest {

	@Test
	public void testAllScmIgnoresProcess123() throws Exception {

		final Result result = JUnitCore.runClasses(AllScmIgnores123Test.class);

		assertEquals(2, result.getFailureCount());

		assertEquals(0, result.getIgnoreCount());

		assertEquals(3, result.getRunCount());
	}

	@Test
	public void testAllScmIgnoresProcess1() throws Exception {

		final Result result = JUnitCore.runClasses(AllScmIgnores1Test.class);

		assertEquals(0, result.getFailureCount());

		assertEquals(0, result.getIgnoreCount());

		assertEquals(3, result.getRunCount());
	}

	@Test
	public void testAllScmIgnoresProcess1Crash() throws Exception {

		final Result result = JUnitCore.runClasses(AllScmIgnores1CrashTest.class);

		assertEquals(1, result.getFailureCount());

		assertEquals(0, result.getIgnoreCount());

		assertEquals(4, result.getRunCount());
	}

	@Test
	public void testAllScmIgnoresProcess2() throws Exception {

		final Result result = JUnitCore.runClasses(AllScmIgnores2Test.class);

		assertEquals(1, result.getFailureCount());

		assertEquals(0, result.getIgnoreCount());

		assertEquals(3, result.getRunCount());
	}

	@Test
	public void testAllScmIgnoresProcess3() throws Exception {

		final Result result = JUnitCore.runClasses(AllScmIgnores3Test.class);

		assertEquals(1, result.getFailureCount());

		assertEquals(0, result.getIgnoreCount());

		assertEquals(3, result.getRunCount());
	}
}
