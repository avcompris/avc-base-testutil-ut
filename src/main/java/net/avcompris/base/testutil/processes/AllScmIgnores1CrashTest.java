package net.avcompris.base.testutil.processes;

import org.junit.Test;

@RequiresProcesses(ExtractAllScmIgnoresProcess1Crash.class)
public class AllScmIgnores1CrashTest extends AllScmIgnoresXXXTest {

	@Test
	public void testCrashOnProject1() throws Exception {

		final ScmIgnore ignore = getProcessCurrentOfType(ScmIgnore.class);

		if (ignore.projectDirName.equals("project1")) {

			throw new RuntimeException("CRASH");
		}
	}
}
