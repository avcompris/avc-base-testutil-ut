package net.avcompris.base.testutil.processes;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;

public abstract class AllScmIgnoresXXXTest extends TestsWithProcessesBefore {

	private static final String[] MANDATORY_IGNORES = new String[] { "target",
			".project", ".classpath", ".settings" };

	private static final Collection<String> RESTRICT_TO_IGNORES = Arrays
			.asList("target", ".project", ".classpath", ".settings", "bin");

	@Test
	@WhileProcessing
	public final void testAllScmIgnoresHaveTargetEtc() throws Exception {

		final ScmIgnore ignore = getProcessCurrentOfType(ScmIgnore.class);

		for (final String a : MANDATORY_IGNORES) {

			assertTrue(ignore.projectDirName + ":ignore should contain " + a,
					ignore.ignores.contains(a));
		}
	}

	@Test
	@WhileProcessing
	public final void testAllScmIgnoresHaveOnlyTargetEtc() throws Exception {

		final ScmIgnore ignore = getProcessCurrentOfType(ScmIgnore.class);

		for (final String a : ignore.ignores) {

			assertTrue(ignore.projectDirName + ":ignore should not contain "
					+ a, RESTRICT_TO_IGNORES.contains(a));
		}
	}

	@Test
	@WillReportAfterProcesses
	public final void testTestsOnAllIgnores() throws Exception {

		final ScmIgnore[] ignores = getProcessResultOfType(ScmIgnore[].class);
	}
}
