package net.avcompris.base.testutil.processes;

import java.util.Collection;

class ScmIgnore implements ProcessEntry {

	public ScmIgnore(final String projectDirName, final boolean isSvn,
			final boolean isHg, final Collection<String> ignores) {

		this.projectDirName = projectDirName;
		this.isSvn = isSvn;
		this.isHg = isHg;
		this.ignores = ignores;
	}

	public final String projectDirName;

	public final boolean isSvn;

	public final boolean isHg;

	public final Collection<String> ignores;
	
	public String getProcessEntryId() {
	    
	    return projectDirName;
	}
}
